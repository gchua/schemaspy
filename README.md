
### Overview

SchemaSpy is a Java-based tool that analyzes the metadata of a schema in a database and generates a visual representation of it in HTML format. This project is a quick starter to generate the report.

* Releases - https://github.com/schemaspy/schemaspy/releases/
* Documentation - https://schemaspy.readthedocs.io/en/latest/
* Sample Output - http://schemaspy.org/sample/index.html

### Tested

* MySQL 8.0.13 with SchemaSpy 6.0.0